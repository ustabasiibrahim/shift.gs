<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="ShiftGS">
    <title>ShiftGS</title>

    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/full-slider.css" rel="stylesheet">
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="css/custom.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<div class="visible-xs">
  <div class="disHeader">
    <div style="width:110px; float: left;">
        <img src="images/logo.png" class="img-responsive">
    </div>
        <div class="social-icons" style="width: 90px; float: right; margin-top:10px;">
            <a href="http://facebook.com/shiftgs" target="_blank"><img src="images/facebook.png"></a>
            <a href="http://twitter.com/shiftgs" target="_blank"><img src="images/twitter.png"></a>
            <a href="#" data-toggle="modal" data-target=".bs-example-modal-lg" ><img src="images/contact1.png"></a>
        </div>
  </div>
</div>

@if(Session::has('message'))
  <div class="message">
    <i class="glyphicon glyphicon-remove" style="top: 2px;cursor: pointer;" id="closeMsg"></i>{{Session::get('message')}}
  </div>
@endif

    <!-- Full Page Image Background Carousel Header -->
    <header id="myCarousel" class="carousel slide">

        <!-- Indicators -->


        <!-- Wrapper for Slides -->
        <div class="carousel-inner">

            <div class="item" id="pengpeng">
                <!-- Set the first background image using inline CSS below. -->
                <div class="fill">
                    <img src="http://www.mrwallpaper.com/wallpapers/crysis-3-game-1920x1080.jpg" id="pengpeng-image">
                </div>
                <div class="carousel-caption">
                <h2>PengPeng</h2>

                    <a href="#" target="_blank"><img src="images/googleplay.png" alt=""></a>
                    <a href="#" target="_blank"><img src="images/appstore.png" alt=""></a>
                </div>
            </div>

            <div class="item" id="robot">
                <!-- Set the first background image using inline CSS below. -->
                <div class="fill">
                    <img src="http://eskipaper.com/images/tumblr-backgrounds-9.jpg">
                </div>
                <div class="carousel-caption">
                <h2>Hero Robot</h2>

                    <a href="#" target="_blank"><img src="images/googleplay.png" alt=""></a>
                    <a href="#" target="_blank"><img src="images/appstore.png" alt=""></a>
                </div>
            </div>

            <div class="item" id="shift">
                <!-- Set the first background image using inline CSS below. -->
                <div class="fill">
                    <img src="http://eskipaper.com/images/tumblr-backgrounds-13.jpg">
                </div>
                <div class="carousel-caption">
                <h2>SHIFT Game</h2>
                    <a href="#" target="_blank"><img src="images/googleplay.png" alt=""></a>
                    <a href="#" target="_blank"><img src="images/appstore.png" alt=""></a>
                </div>
            </div>
        </div>
  </header>

       <div class="container-fluid">
          <div class="row">
            <div class="footer">
                <div class="col-md-9">
                    <div class="owl-carousel">
                      <a href="pengpeng" >
                        <img src="http://www.mrwallpaper.com/wallpapers/crysis-3-game-1920x1080.jpg" alt="" data-slide-to="0" data-target="#myCarousel">
                        <div class="imgContainer" data-slide-to="0" data-target="#myCarousel" id="pengpeng-slider">
                          <div class="imgTopBg">

                          </div>
                          <div class="imgBottomBg">
                            <div></div>
                          </div>
                          <p>PengPeng</p>
                        </div>
                      </a>
                       <a href="robot">
                        <img src="http://eskipaper.com/images/tumblr-backgrounds-9.jpg" alt="" data-slide-to="1" data-target="#myCarousel">
                        <div class="imgContainer" data-slide-to="1" data-target="#myCarousel" id="robot-slider">
                          <div class="imgTopBg">

                          </div>
                          <div class="imgBottomBg">
                              <div></div>
                          </div>
                          <p>Hero Robo</p>

                        </div>

                       </a>
                      <a href="shift">
                        <img src="http://eskipaper.com/images/tumblr-backgrounds-13.jpg" alt="" data-slide-to="2" data-target="#myCarousel">
                        <div class="imgContainer" data-slide-to="2" data-target="#myCarousel" id="shift-slider">
                          <div class="imgTopBg">

                          </div>
                          <div class="imgBottomBg">
                            <div></div>
                          </div>
                          <p>SHIFT Game</p>

                        </div>

                      </a>
                    </div>
                </div>
                <div class="col-md-2 right-col">
                    <div class="shift-logo">
                        <img src="images/logo.png" class="img-responsive">
                        <div class="social-icons">
                            <a href="http://facebook.com/shiftgs" target="_blank"><img src="images/facebook.png"></a>
                            <a href="http://twitter.com/shiftgs" target="_blank"><img src="images/twitter.png"></a>
                            <a href="#" data-toggle="modal" data-target=".bs-example-modal-lg" ><img src="images/contact1.png"></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="stores">
                        <img src="images/appstore.png" alt="" class="img-responsive">
                        <img src="images/googleplay.png" alt="" class="img-responsive">
                    </div>
                </div>
            </div>
          </div>
        </div>
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="icon-next"></span>
        </a>



    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="font-family: sans-serif;" id="mod">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Contact</h4>
          </div>
          <div class="modal-body">
            <div class="row">

            <div class="col-md-6">
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3007.694226902863!2d29.016116315695953!3d41.075676023223124!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14cab641e76f5fd5%3A0x189334157e755f1d!2zS2FzxLFtcGF0xLEgU2suIE5vOjEsIExldmVudCwgMzQzMzAgQmXFn2lrdGHFny_EsHN0YW5idWwsIFTDvHJraXll!5e0!3m2!1str!2s!4v1460453260593"  frameborder="0" style="width: 98%; height: 250px;border:0" allowfullscreen></iframe>
              <br><br><p><b>Address:</b> Nispetiye Caddesi, Kasımpatı sokak. No:4 Kat:2  1.Levent / Beşiktaş / İstanbul </p>
            </div>
            <div class="col-md-6">
              <form class="form-horizontal" method="POST" id="contactForm" action="{{route('email')}}">
                {{ csrf_field() }}
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Name</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="name" name="name" required="required">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">E-mail</label>
                  <div class="col-sm-10">
                    <input type="email" class="form-control" id="email" name="email" required="required">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Message</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" rows="3" id="message" name="message" required="required"></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-10 col-sm-offset-9">
                    <button  type="submit" name="button" class="btn btn-primary" id="submitForm">Submit</button>
                  </div>
                </div>

                <!-- Süleyman  -->

               
               
                    <div class="makeMeDraggable">
                      <img src="/images/playground.png" id="my-image" style="display: none;">
                  
                 </div>
                
                   <div class="captcha-playground " id="target">
                </div>
                  <div class="captcha-playground makeMeDraggable" id="target2">
                </div>
                 <div class="makeMeDraggable"  > 
                    <img src="/images/block11.png" id="my-image2">
                  
                </div>

             
                
             
               <div id="result">
                
              </div>
          
  

                <!-- Süleyman END  -->



              </form>
            </div>
          </div>
          </div>
        </div>
      </div>
    </div>

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script src="js/jquery.mobile.custom.js" charset="utf-8"></script>
    <script src="js/owl.carousel.js"></script>
    <script>
   $('.owl-carousel').owlCarousel({
        loop:false,
        margin:10,
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
                nav:true
            },
            600:{
                items:3,
                nav:false
            },
            1000:{
                items:5,
                nav:true,
                loop:false
            }
        }
    });


    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {

        $(".carousel").swiperight(function() {
            $(this).carousel('prev');
            console.log();
        });
        $(".carousel").swipeleft(function() {
            $(this).carousel('next');
        });
        $('#pengpeng-image').attr('src', '/images/bg.jpg');
    }

   $(".owl-carousel a").click(function() {
        var href = $(this).attr('href');
        history.pushState('','', href);
        $('.imgTopBg').removeClass('actived');
        $('.imgBottomBg').removeClass('actived');
        $('.imgContainer p').removeClass('txt');
        $('.message').hide();

        $(this).find('.imgTopBg').addClass('actived');
        $(this).find('.imgBottomBg').addClass('actived');
        $(this).find('p').addClass('txt');
   });

   $('#closeMsg').click(function(){
     $('.message').hide();
   })

   var base = location.pathname;
   var game = base.substr(1);
   var games = ['pengpeng', 'robot', 'shift'];

   if(games.indexOf(game) !== -1)
   {
       $('#'+game).addClass('active');
       $('#'+game+'-slider').find('.imgTopBg').addClass('actived');
       $('#'+game+'-slider').find('.imgBottomBg').addClass('actived');
       $('#'+game+'-slider').find('p').addClass('txt');
   }
   else{
     $('#pengpeng').addClass('active');
     $('#pengpeng-slider').find('.imgTopBg').addClass('actived');
     $('#pengpeng-slider').find('.imgBottomBg').addClass('actived');
     $('#pengpeng-slider').find('p').addClass('txt');

   }
   $('#submitForm').click(function(e){
     var name = $('#name').val();
     var email = $('#email').val();
     var message = $('#message').val();

     if(name == "" || email == "" || message == "")
     {
       $('#mod').effect('shake');
       $('#name').val('');
       $('#email').email('');
       $('#message').message('');
       e.preventDefault();
     }else
     {

     }
   });

    </script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.0/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/jquery-ui.min.js"></script>
<script src="/js/custom.js"></script>

</body>

</html>
