<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class Straight extends TetrisBlock
{
    public $width  = 4;
    public $height = 1; 
    public $filledBlocks = [ [0,0],[1,0],[2,0],[3,0] ];

}
