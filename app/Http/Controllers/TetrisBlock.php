<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class TetrisBlock extends Controller
{
    public $width = "";
    public $height = "";
    public $filledBlocks = [];

    public function getWidth()
    {
      return $this->width;
    }

    public function getHeight()
    {
      return $this->height;
    }

    public function getFilledBlocks()
    {
      return $this->filledBlocks;
    }
}
