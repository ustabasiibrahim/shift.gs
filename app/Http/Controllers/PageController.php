<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Intervention\Image\ImageManagerStatic as Image;

use App\Http\Requests;

class PageController extends Controller
{
    public function getIndex()
    {
      return view('index');
    }

    public function getGame($game)
    {
      $games = ['', 'pengpeng', 'robot', 'shift'];
      if(in_array($game, $games))
      {
        return View('index');
      }else{
        \Session::flash('message', '404 Not Found');
        return View('index');
      }
    }

    public function postMail(Request $request)
    {
      Mail::send('mailer',
          array(
              'name' => $request->get('name'),
              'email' => $request->get('email'),
              'user_message' => $request->get('message')
          ), function($message)
      {
          $message->from('noreply@shift.tech');
          $message->to('noreply@shift.tech', 'Admin')->subject('Shift GameStudio Contact Form');
      });

    return redirect()->back();
    }

    public function test()
    {
      return $deneme = "asdasdasd";
    }

    public function tester()
    {
      $img = Image::canvas(500, 500, '#d90');
      $img->rectangle(0, 0, 10, 10, function ($draw) {
          $draw->background('rgba(255, 255, 255, 0.5)');
      });
      return $img->response('png');
    }
}
