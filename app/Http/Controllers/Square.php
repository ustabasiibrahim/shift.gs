<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class Square extends TetrisBlock
{
  public $width = 2;
  public $height = 2;
  public $filledBlocks = [ [0,0], [0,1], [1,0], [1,1] ];
}
