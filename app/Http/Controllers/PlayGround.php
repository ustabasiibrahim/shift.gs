<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Requests;

class PlayGround extends Controller
{
    public $width  = 10;
    public $height = 10;
    public $blocks = [];


    public function getRandomFigure()
    {
        $block = array(new T, new S, new L, new Square, new Straight);
        $rand = array_rand($block,1);
        return $this->insertBlock($block[$rand]);
    }

    public function insertBlock($block)
    {

        $xcoordinate = $this->width - $block->width;
        $ycoordinate = $this->height - $block->height;


        $x = rand(0, $xcoordinate);
        $y = rand(0, $ycoordinate);


        $position = $block->getFilledBlocks();      


        $img = Image::canvas($this->width, $this->height);

        $img->rectangle($position[0][0]+$x, $position[0][1]+$y, $position[0][0]+$x, $position[0][1]+$y, function ($draw) {
            $draw->background('#ccc');
        });
         $img->rectangle($position[1][0]+$x,$position[1][1]+$y,$position[1][0]+$x,$position[1][1]+$y, function ($draw) {
            $draw->background('#ccc');
        });
          $img->rectangle($position[2][0]+$x,$position[2][1]+$y,$position[2][0]+$x,$position[2][1]+$y, function ($draw) {
            $draw->background('#ccc');
        });

         $img->rectangle($position[3][0]+$x,$position[3][1]+$y,$position[3][0]+$x,$position[3][1]+$y, function ($draw) {
            $draw->background('#ccc');
        });

        return $img->response('png');

    }    
}
