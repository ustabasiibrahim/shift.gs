<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply teull Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['middleware' => ['web']], function () {

  Route::get('deneme','PlayGround@getRandomFigure');


  Route::get('test', 'PlayGround@insertBlock');
  Route::post('/email', [
    'as' => 'email', 'uses' => 'PageController@postMail'
  ]);


  Route::get('/', 'PageController@getIndex');
  Route::get('/{game}', 'PageController@getGame');




});
//Route::post('/email', ['as' => 'email', 'uses' => 'PageController@postMail']);
